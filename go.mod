module gitlab.com/groenator/gcloud-cli

go 1.14

require (
	cloud.google.com/go v0.57.0
	github.com/prometheus/common v0.9.1
	google.golang.org/genproto v0.0.0-20200511104702-f5ebc3bea380
)

package create

import (
	container "cloud.google.com/go/container/apiv1"
	"context"
	"fmt"
	"github.com/prometheus/common/log"
	containerpb "google.golang.org/genproto/googleapis/container/v1"
)

func ProvisioningCluster(err error, clusterClient *container.ClusterManagerClient, ctx context.Context, clusterConfiguration *containerpb.CreateClusterRequest) {

	//Create Cluster
	createCluster, _ := clusterClient.CreateCluster(ctx, clusterConfiguration)
	if err != nil {
		log.Fatalf("error in submitting the cluster creation request: %v\n", err)
	}

	//View Cluster Status
	operationStatus := createCluster.Name
	if err != nil {
		log.Fatalf("An error occurred in creating the request to build cluster: %q: %v\n", operationStatus, err)
	} else {
		fmt.Println("The request to create the cluster was made successfully:", clusterConfiguration.Cluster.Name)
	}
}

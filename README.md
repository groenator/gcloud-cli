# google-gke-cli

A small tool to provision or destroy a kubernetes cluster in gCloud. 
The intention of this project is to create a small cli which will help to provision or destroy k8s clusters in gCloug.
At moment the project is in alpha mode.

## Provisioning a Google container (k8s) cluster

* CLI Flags
  * project_id 
  * region
  * cluster_name
  * version
  * machine_type
  * cluster_operation

```bash
./gcloud-cli-{os} -project_id wonkypaw -region europe-west2-a -cluster_name test-cluster -version 1.15.12-gke.2 -machine_type e2-medium -cluster_operation create
```

### Destroying a Google container (k8s) cluster

* CLI Flags
  * project_id
  * region
  * cluster_name

```bash
./gcloud-cli-{os} -project_id wonkypaw -region europe-west2-a -cluster_name test-cluster -cluster_operation destroy
````

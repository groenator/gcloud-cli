# gCloud GKE Cluster Configuration Management Changelog
## [1.6.0](https://gitlab.com/groenator/google-cloud-gke/compare/v1.5.0...v1.6.0) (2020-08-10)


### Documentation

* **readme:** updating readme ([a69f029](https://gitlab.com/groenator/google-cloud-gke/commit/a69f0296da64f1e7309e16d403e251f9e6f19112))

## [1.5.0](https://gitlab.com/groenator/google-cloud-gke/compare/v1.4.0...v1.5.0) (2020-08-10)


### Bug Fixes

* **gitignore:** ignore to upload binaries into the repo ([0bf17ad](https://gitlab.com/groenator/google-cloud-gke/commit/0bf17ad3100c2a7314244467802706238357b6ee))

## [1.4.0](https://gitlab.com/groenator/google-cloud-gke/compare/v1.3.0...v1.4.0) (2020-08-10)


### Refactors

* **decommissioning:** adding decommissioning input struct ([a89c4a6](https://gitlab.com/groenator/google-cloud-gke/commit/a89c4a6f09010cc2aa329d81c08be83a9025e0e0))
* **provisioning:** adding provisioning input struct ([a4b7255](https://gitlab.com/groenator/google-cloud-gke/commit/a4b7255f9279e7a4834685997e5f6ab9416929a7))

## [1.3.0](https://gitlab.com/groenator/google-cloud-gke/compare/v1.2.0...v1.3.0) (2020-08-10)


### Features

* **version:** adding manifest ([3fc94ee](https://gitlab.com/groenator/google-cloud-gke/commit/3fc94ee7d0317d6e840af47f9a18ed66f7a44f16))


### Bug Fixes

* **standard:** adding a comprehensive changelog ([9dea8ae](https://gitlab.com/groenator/google-cloud-gke/commit/9dea8aedee8432198862b741f715d0cf3743dc89))


### Chore

* **release:** 1.3.0 ([02cb145](https://gitlab.com/groenator/google-cloud-gke/commit/02cb14502010526b3b70ac91a614900f9d9bd996))


### Documentation

* **readme:** adding release process documentations ([f4fcbe1](https://gitlab.com/groenator/google-cloud-gke/commit/f4fcbe1dcdea0204f221ebb6ff00eb6179571c5c))
* **readme:** update the release docs ([cdeb6c1](https://gitlab.com/groenator/google-cloud-gke/commit/cdeb6c1ce79f5aec82dd7680e5756ab81cad41f7))
* **release:** updating release process ([42386b3](https://gitlab.com/groenator/google-cloud-gke/commit/42386b322ea9876078d8cffb86ac6a42e77ab3bf))

## [1.3.0](https://gitlab.com/groenator/google-cloud-gke/compare/v1.2.0...v1.3.0) (2020-08-10)

## [1.2.0](https://gitlab.com/groenator/google-cloud-gke/compare/v1.1.0...v1.2.0) (2020-08-10)


### Features

* **binary:** creating binaries for Linux and Mac ([57d8e92](https://gitlab.com/groenator/google-cloud-gke/commit/57d8e9268d170f5cedb709f2b03e229da5b52670))

## 1.1.0 (2020-08-10)


### Features

* **gcloud:** adding readme ([968f8d8](https://gitlab.com/groenator/google-cloud-gke/commit/968f8d85b8a19d809b717ffb12be1c032f878a6c))

## 1.1.0 (2020-08-10)


### Features

* **gcloud:** adding readme ([968f8d8](https://gitlab.com/groenator/google-cloud-gke/commit/968f8d85b8a19d809b717ffb12be1c032f878a6c))

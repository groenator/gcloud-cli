package client

import (
	container "cloud.google.com/go/container/apiv1"
	"context"
	"github.com/prometheus/common/log"
)

func ClusterClient() (context.Context, *container.ClusterManagerClient, error) {
	ctx := context.Background()

	// Create client
	clusterClient, err := container.NewClusterManagerClient(ctx)

	if err != nil {
		log.Fatal("Failed to create cluster client: %v", err)
	}
	return ctx, clusterClient, err
}


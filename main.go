package main

import (
	"flag"
	. "gitlab.com/groenator/gcloud-cli/client"
	. "gitlab.com/groenator/gcloud-cli/config"
	. "gitlab.com/groenator/gcloud-cli/create"
	. "gitlab.com/groenator/gcloud-cli/destroy"
)


func main() {

	var projectID, clusterName, region, version, machineType, clusterOperation string
	flag.StringVar(&projectID, "project_id", "", "Cloud Project ID, used for creating resources.")
	flag.StringVar(&clusterName, "cluster_name", "", "The name of the cluster")
	flag.StringVar(&region, "region", "", "The region where the cluster will be built")
	flag.StringVar(&version, "version", "", "The k8s version used to build the cluster")
	flag.StringVar(&machineType, "machine_type", "", "The type of the machine used")
	flag.StringVar(&clusterOperation, "cluster_operation", "", "Create the cluster")
	flag.Parse()

	ctx, clusterClient, err := ClusterClient()

	createReq := ProvisionClusterConfig(&ProvisionPropertiesInput{
		ProjectID: projectID,
		ClusterName: clusterName,
		Region: region,
		Version: version,
		MachineType: machineType,
		ClusterOperation: clusterOperation,
		})
	if clusterOperation == "create" {
		ProvisioningCluster(err, clusterClient, ctx, createReq)
	}

	destroyRequest := DestroyClusterConfig(&ProvisionPropertiesInput{
		ProjectID:        projectID,
		Region:           region,
		ClusterName:      clusterName,
		ClusterOperation: clusterOperation,
	})
	if clusterOperation == "destroy" {
		DestroyingCluster(clusterClient, ctx, err, destroyRequest)
	}
}

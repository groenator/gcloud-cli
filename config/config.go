package config

import containerpb "google.golang.org/genproto/googleapis/container/v1"

type EnvironmentProperties struct {
	ProvisionProperties *ProvisionPropertiesInput
}

type ProvisionPropertiesInput struct {
	ProjectID        string
	Region           string
	ClusterName      string
	Version          string
	MachineType      string
	ClusterOperation string
}

func ProvisionClusterConfig(properties *ProvisionPropertiesInput) *containerpb.CreateClusterRequest {

	//Set configuration for provisioning the cluster
	provisionClusterRequest := &containerpb.CreateClusterRequest{
		ProjectId: properties.ProjectID,
		Zone:      properties.Region,
		Cluster: &containerpb.Cluster{
			Name:                  properties.ClusterName,
			Zone:                  properties.Region,
			InitialClusterVersion: properties.Version,
			InitialNodeCount:      1,
			NodeConfig: &containerpb.NodeConfig{
				MachineType: properties.MachineType,
				DiskSizeGb:  20,
			},
		},
	}
	return provisionClusterRequest
}

func DestroyClusterConfig(properties *ProvisionPropertiesInput) *containerpb.DeleteClusterRequest {
	//Set configuration for destroying the cluster
	destroyClusterRequest := &containerpb.DeleteClusterRequest{
		ProjectId: properties.ProjectID,
		Zone:      properties.Region,
		ClusterId: properties.ClusterName,
	}
	return destroyClusterRequest
}

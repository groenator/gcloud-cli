# How to create a release

Our release process is using standard-version format or Semantic version - [Semantic Version](https://semver.org/) format of vMAJOR.MINOR.PATCH, e.g. v1.0.2.

## Pre-requisites

```bash
npm install -g standard-version
```

A file called [`manifest.json`](https://gitlab.com/groenator/google-cloud-gke/-/blob/master/manifest.json) keeps track of the version number. When creating a release the `standard-version` tool will automatically bump this to the next version depending on the types of commits that go into the release.

If you do not add any additional features (commits messages of type `feat(something): blah`) then the release will be considered a "patch". e.g. v1.0.2 -> v1.0.3.

If you add features the release will be considered a "minor". e.g. v1.0.2 -> v1.1.0

### Step 1: Checkout master

Checkout a clean version of this repo and switch to the master branch `git status`

```bash
git clone git@gitlab.com:groenator/google-cloud-gke.git
git checkout master
git status
    On branch master
    Your branch is up to date with 'origin/master'

    nothing to commit, working tree clean
```

## Step 2: Fetch all tags

Fetch all tags

```bash
git fetch --tags
git tag
```

Verify that all tags are annotated and not lightweight (https://stackoverflow.com/questions/11514075/what-is-the-difference-between-an-annotated-and-unannotated-tag)

If any return a message: `error: v0.2.0: cannot verify a non-tag object of type commit.`

```bash
git tag | xargs git tag -v
```

If any return a message: `error: v0.2.0: cannot verify a non-tag object of type commit.` Then run the following:

```bash
tag=$1
date="$(git show $tag --format=%aD | head -1)"
GIT_COMMITTER_DATE="$date" git tag -a -f $tag $tag^0
```

### Step 3: Dry Run release and verify duplicate entries

Create a dry run

```bash
standard-version --release-as minor --changelogHeader "# gCloud GKE Cluster Configuration Management Changelog" --dry-run
```

_**NOTE: only run this on the root of the google-cloud-gke directory**_

Do a 'dry run' of the "release" first

If everything looks good in the dry run, continue with the next step to push the release to gitlab
However, if you find any duplicate entries or issues with the release fix the issues and re-run the standard-version --dry-run command again, until all your issues are fixed

_**NOTE: do not try to modify the files and create manual commits, if you do that the standard-version will ignore your commit and tags**_

```bash
[standard-version]: --changelogHeader will be removed in the next major release. Use --header.
✔ outputting changes to CHANGELOG.md

---
## [1.3.0](https://gitlab.com/groenator/google-cloud-gke/compare/v1.2.0...v1.3.0) (2020-08-10)


### Features

* **version:** adding manifest ([3fc94ee](https://gitlab.com/groenator/google-cloud-gke/commit/3fc94ee7d0317d6e840af47f9a18ed66f7a44f16))
---

✔ committing CHANGELOG.md
✔ tagging release v1.3.0
ℹ Run `git push --follow-tags origin master` to publish
```

Compare the commits of the new tag vs the previous tag with the dry-run output to verify that it is correct.

```bash
git log v1.1.0...v1.2.0 --oneline
```

Proceed with the next step and create a new tag for the upcoming release

```bash
standard-version --release-as minor --changelogHeader "# gCloud GKE Cluster Configuration Management Changelog"
```

### Step 4: Push the release to Gitlab

Push the release tag to Github.

```bash
git push --follow-tags origin master
```

### Step 5: Publish the release

- Go to https://gitlab.com/groenator/google-cloud-gke/-/tags and check the new tags is there
- Go to https://gitlab.com/groenator/google-cloud-gke/-/releases
- Click 'Edit release notes (In tags)'
- Enter the tag and title like 'v0.5.0 Beta release'
- Copy the contents of the changelog for this release into the description
- Then 'Preview' to ensure the format is correct and click save
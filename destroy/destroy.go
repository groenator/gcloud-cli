package destroy

import (
	"cloud.google.com/go/container/apiv1"
	"github.com/prometheus/common/log"
	"context"
	"fmt"
	containerpb "google.golang.org/genproto/googleapis/container/v1"
)

func DestroyingCluster(clusterClient *container.ClusterManagerClient, ctx context.Context, err error, clusterConfiguration *containerpb.DeleteClusterRequest) {

	destroyCluster, _ := clusterClient.DeleteCluster(ctx, clusterConfiguration)

	if err != nil {
		log.Fatalf("An error occurred in submitting the request to delete %q: %v\n", destroyCluster, err)
	}
	fmt.Printf("The request to destroy %q cluster was made successfully", clusterConfiguration.ClusterId)
}
